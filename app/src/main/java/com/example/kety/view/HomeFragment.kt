package com.example.kety.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kety.adapter.CategoryAdapter
import com.example.kety.adapter.DetailAdapter
import com.example.kety.databinding.FragmentTestBinding
import com.example.kety.viewmodel.KetyViewModel
import com.example.kety.viewmodel.SkinViewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentTestBinding? = null
    private val binding get() = _binding!!
    private val ketyViewModel by viewModels<KetyViewModel>()
    private val skinViewModel by viewModels<SkinViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTestBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        with(binding){
//            ketyViewModel.getCategory()
//            rvDetails.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
//            rvDetails.adapter = CategoryAdapter()
//            ketyViewModel.category.observe(viewLifecycleOwner){
//                val adapter = rvDetails.adapter
//                if (adapter is CategoryAdapter){
//                    adapter.addCategorys(it)
//                }
//            }
//        }
        displayCategory()
        displaySkin()
    }

    fun displayCategory(){
        with(binding.rvFirst){
            ketyViewModel.getCategory()
            layoutManager=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
            adapter = CategoryAdapter()
            ketyViewModel.category.observe(viewLifecycleOwner){
                val adapter = adapter
                if (adapter is CategoryAdapter){
                    adapter.addCategorys(it)
                }
            }
        }
    }

    fun displaySkin(){
        with(binding.rvSecond){
            layoutManager=LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL,false)
            adapter= DetailAdapter().apply {
                with(skinViewModel){
                    getSkin()
                    skin.observe((viewLifecycleOwner)){
                        addSkins(it)
                    }
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}