package com.example.kety.model.remote

interface SkinService {

    suspend fun getSkin(): List<String>
}