package com.example.kety.model.remote

interface KetyService {

    suspend fun getCategory(): List<String>
}