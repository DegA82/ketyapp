package com.example.kety.model

import com.example.kety.R
import com.example.kety.model.remote.KetyService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object KetyRepo {

    private val ketyService = object : KetyService{
        override suspend fun getCategory(): List<String> {
            return listOf("Ranked","Loved","Hot", "Secrete", "Cold", "Freezing", "Kotlin", "Java", "Python", "MERN" )

//            return listOf(
//                Pair("Ranked", R.drawable.rank),
//                Pair("Loved", R.drawable.loved),
//                Pair("Hot", R.drawable.hot),
//                Pair("Secrete", R.drawable.secret),
//            )
        }
    }

    suspend fun getCategory(): List<String> = withContext(Dispatchers.IO){
        ketyService.getCategory()
    }



}