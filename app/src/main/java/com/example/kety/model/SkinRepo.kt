package com.example.kety.model


import com.example.kety.model.remote.KetyService
import com.example.kety.model.remote.SkinService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object SkinRepo {

    private val skinService = object : SkinService {
        override suspend fun getSkin(): List<String> {
            return listOf("Ranked","Loved","Hot", "Secrete", "Cold", "Freezing", "Kotlin", "Java", "Python", "MERN" )

        }
    }

    suspend fun getSkin(): List<String> = withContext(Dispatchers.IO){
        skinService.getSkin()
    }


}