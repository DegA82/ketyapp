package com.example.kety.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemDetailsBinding
import com.example.kety.view.HomeFragmentDirections


class DetailAdapter: RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {

    private var skins = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType:Int): DetailViewHolder {
        val binding = ItemDetailsBinding.inflate(
            LayoutInflater.from(parent.context), parent,false)
        return DetailViewHolder(binding)

    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        val skin = skins[position]
        holder.bindCategory(skin)
    }

    override fun getItemCount(): Int {
        return skins.size
    }

    fun addSkins(name: List<String>){
        this.skins = name.toMutableList()
        notifyDataSetChanged()
    }

    class DetailViewHolder(
        private val binding: ItemDetailsBinding
    ): RecyclerView.ViewHolder(binding.root){

        fun bindCategory(name: String){
            binding.tvSkin.text = name
            binding.tvSkin  .setOnClickListener {
                it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDisplayFragment(name))
            }


        }
    }
}