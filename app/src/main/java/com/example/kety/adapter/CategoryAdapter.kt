package com.example.kety.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemHomeBinding
import com.example.kety.view.HomeFragmentDirections

class CategoryAdapter: RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categorys = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType:Int): CategoryViewHolder {
       val binding = ItemHomeBinding.inflate(
           LayoutInflater.from(parent.context), parent,false)
        return CategoryViewHolder(binding)

    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categorys[position]
        holder.bindCategory(category)
    }

    override fun getItemCount(): Int {
        return categorys.size
    }

    fun addCategorys(name: List<String>){
        this.categorys = name.toMutableList()
        notifyDataSetChanged()
    }

    class CategoryViewHolder(
        private val binding: ItemHomeBinding
    ): RecyclerView.ViewHolder(binding.root){

        fun bindCategory(name: String){
            binding.tvInformation.text = name

           binding.ivCategory.setOnClickListener {
               it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDisplayFragment(name))

            }


        }
    }
}