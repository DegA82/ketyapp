package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.SkinRepo

import kotlinx.coroutines.launch

class SkinViewModel:ViewModel() {

    private val repoSkin = SkinRepo

    private val _skin = MutableLiveData <List<String>>()
    val skin: LiveData<List<String>> get() = _skin

    fun getSkin() {
        viewModelScope.launch {
            _skin.value = repoSkin.getSkin()
        }
    }


}