package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.KetyRepo
import kotlinx.coroutines.launch

class KetyViewModel: ViewModel() {

    private val repo = KetyRepo

    private val _category = MutableLiveData <List<String>>()
    val category: LiveData<List<String>> get() = _category

    fun getCategory() {
        viewModelScope.launch {
            _category.value = repo.getCategory()
        }
    }
}